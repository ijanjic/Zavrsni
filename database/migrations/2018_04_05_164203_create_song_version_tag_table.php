<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongVersionTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_version_tag', function (Blueprint $table) {
            $table->integer('song-version-id')->unsigned()->index();
            $table->integer('tag-id')->unsigned()->index();
            $table->foreign('song-version-id')
                  ->references('id')
                  ->on('song_versions')
                  ->onDelete('cascade');
            $table->foreign('tag-id')
                  ->references('id')
                  ->on('tags')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_version_tag');
    }
}
