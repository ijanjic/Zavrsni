<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concerts', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('location');
            $table->integer('type-id')->unsigned()->index();
            $table->time('duration');
            $table->string('client');
            $table->string('contact-number');
            $table->integer('price');
            $table->integer('music-group-id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('type-id')
                  ->references('id')
                  ->on('types');
            $table->foreign('music-group-id')
                  ->references('id')
                  ->on('music_groups')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concerts');
    }
}
