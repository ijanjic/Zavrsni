<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $data = array(
            array('name'=>'Pop'),
            array('name'=>'Rock'),
            array('name'=>'Folk'),
            array('name'=>'Jazz'),
            array('name'=>'Techno'),
            array('name'=>'Metal'),
            array('name'=>'Spiritual'),
            array('name'=>'Blues'),
            array('name'=>'Funk'),
            array('name'=>'Hip hop'),
            array('name'=>'Punk'),
            array('name'=>'Classical')
        );
        DB::table('tags')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
