<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('invite-hash');
            $table->integer('admin-id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('admin-id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_groups');
    }
}
