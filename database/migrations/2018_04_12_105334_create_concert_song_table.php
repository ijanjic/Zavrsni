<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcertSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concert_song', function (Blueprint $table) {
            $table->integer('concert-id')->unsigned()->index();
            $table->integer('song-id')->unsigned()->index();
            $table->integer('ordinal-number');
            $table->foreign('concert-id')
                  ->references('id')
                  ->on('concerts')
                  ->onDelete('cascade');
            $table->foreign('song-id')
                  ->references('id')
                  ->on('songs')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concert_song');
    }
}
