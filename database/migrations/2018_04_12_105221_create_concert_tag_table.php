<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcertTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concert_tag', function (Blueprint $table) {
            $table->integer('concert-id')->unsigned()->index();
            $table->integer('tag-id')->unsigned()->index();
            $table->foreign('concert-id')
                  ->references('id')
                  ->on('concerts')
                  ->onDelete('cascade');
            $table->foreign('tag-id')
                  ->references('id')
                  ->on('tags')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concert_tag');
    }
}
