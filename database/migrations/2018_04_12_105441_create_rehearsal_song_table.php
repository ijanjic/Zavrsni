<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRehearsalSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rehearsal_song', function (Blueprint $table) {
            $table->integer('rehearsal-id')->unsigned()->index();
            $table->integer('song-id')->unsigned()->index();
            $table->integer('ordinal-number');
            $table->foreign('rehearsal-id')
                  ->references('id')
                  ->on('rehearsals')
                  ->onDelete('cascade');
            $table->foreign('song-id')
                  ->references('id')
                  ->on('songs')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rehearsal_song');
    }
}
