<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('song-id')->unsigned()->index();
            $table->string('youtube')->nullable();
            $table->string('chordify')->nullable();
            $table->foreign('song-id')
                  ->references('id')
                  ->on('songs')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_versions');
    }
}
