<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tonalities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $data = array(
            array('name'=>'C♭'),
            array('name'=>'G♭'),
            array('name'=>'D♭'),
            array('name'=>'A♭'),
            array('name'=>'E♭'),
            array('name'=>'B'),
            array('name'=>'F'),
            array('name'=>'C'),
            array('name'=>'G'),
            array('name'=>'D'),
            array('name'=>'A'),
            array('name'=>'E'),
            array('name'=>'H'),
            array('name'=>'F#'),
            array('name'=>'C#'),
            array('name'=>'a♭'),
            array('name'=>'e♭'),
            array('name'=>'b'),
            array('name'=>'f'),
            array('name'=>'c'),
            array('name'=>'g'),
            array('name'=>'d'),
            array('name'=>'a'),
            array('name'=>'e'),
            array('name'=>'h'),
            array('name'=>'f#'),
            array('name'=>'c#'),
            array('name'=>'g#'),
            array('name'=>'d#'),
            array('name'=>'a#'),
        );
        DB::table('tonalities')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tonalities');
    }
}
