<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicGroupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_group_user', function (Blueprint $table) {
            $table->integer('music-group-id')->unsigned()->index();
            $table->integer('user-id')->unsigned()->index();
            $table->foreign('music-group-id')
                  ->references('id')
                  ->on('music_groups')
                  ->onDelete('cascade');
            $table->foreign('user-id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_group_user');
    }
}
