<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongVersionTonalityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_version_tonality', function (Blueprint $table) {
            $table->integer('song-version-id')->unsigned()->index();
            $table->integer('tonality-id')->unsigned()->index();
            $table->foreign('song-version-id')
                  ->references('id')
                  ->on('song_versions')
                  ->onDelete('cascade');
            $table->foreign('tonality-id')
                  ->references('id')
                  ->on('tonalities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_tonality');
    }
}
