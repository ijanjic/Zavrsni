<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongMusicGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_music_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('song-id')->unsigned()->index();
            $table->integer('music-group-id')->unsigned()->index();
            $table->integer('song-version-id')->unsigned()->index()->nullable();
            $table->foreign('song-id')
                  ->references('id')
                  ->on('songs')
                  ->onDelete('cascade');
            $table->foreign('music-group-id')
                  ->references('id')
                  ->on('music_groups')
                  ->onDelete('cascade');
            $table->foreign('song-version-id')
                  ->references('id')
                  ->on('song_versions')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('song_music_group');
    }
}
