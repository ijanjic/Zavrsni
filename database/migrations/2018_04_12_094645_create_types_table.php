<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $data = array(
            array('name'=>'Birthday'),
            array('name'=>'Wedding'),
            array('name'=>'Café'),
            array('name'=>'Baptism'),
            array('name'=>'Chrism'),
            array('name'=>'Public'),
            array('name'=>'Other'),
        );
        DB::table('types')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
