<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    protected $fillable = [
        'name', 'text'
    ];

    public function musicGroups()
    {
        return $this->belongsToMany('App\MusicGroup', 'song_music_group', 'song-id', 'music-group-id');
    }

    public function versions()
    {
        return $this->hasMany('App\SongVersion', 'song-id', 'id');
    }

}
