<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongVersion extends Model
{
      protected $fillable = [
          'youtube', 'chordify', 'song-id'
      ];

      public function song()
      {
       return $this->belongsTo('App\Song', 'song-id', 'id');
      }

      public function tonalities()
      {
          return $this->belongsToMany('App\Tonality', 'song_version_tonality', 'song-version-id', 'tonality-id');
      }

      public function tags()
      {
          return $this->belongsToMany('App\Tag', 'song_version_tag', 'song-version-id', 'tag-id');
      }
}
