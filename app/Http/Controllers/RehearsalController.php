<?php

namespace App\Http\Controllers;

use App\MusicGroup;
use App\Rehearsal;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RehearsalController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myGroups = \Auth::user()->musicGroups()->pluck('id')->toArray();
        $rehearsals = Rehearsal::whereIn('music-group-id', $myGroups)->orderBy('date', 'asc')->paginate(10);
        return view('rehearsals.index', compact('rehearsals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $musicGroups = \Auth::user()->musicGroups()->get();
        return view('rehearsals.create', compact('musicGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'date' => 'required',
            'duration' => 'required',
            'music-group-id' => 'required',
            'song_order' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $rehearsal = Rehearsal::create($data);
        $data['song_order'] = explode(",", $data['song_order']);

        foreach ($data['song_order'] as $idx => $song) {
            $rehearsal->songs()->save(Song::find($song), ['ordinal-number' => $idx]);
        }

        return redirect()->back()->with('status', 'Rehearsal successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rehearsal  $rehearsal
     * @return \Illuminate\Http\Response
     */
    public function show(Rehearsal $rehearsal)
    {
        if(!MusicGroupController::userInMusicGroup($rehearsal->musicGroup()->first())) {
            return redirect()->back();
        }
        return view('rehearsals.show', compact('rehearsal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rehearsal  $rehearsal
     * @return \Illuminate\Http\Response
     */
    public function edit(Rehearsal $rehearsal)
    {
        if(!MusicGroupController::userInMusicGroup($rehearsal->musicGroup()->first())) {
            return redirect()->back();
        }
        $musicGroups = \Auth::user()->musicGroups()->get();
        $rehearsal->date = \Carbon\Carbon::parse($rehearsal->date)->format('Y-m-d') . "T" . \Carbon\Carbon::parse($rehearsal->date)->format('H:i');
        $rehearsal->duration = \Carbon\Carbon::parse($rehearsal->duration)->format('H:i');
        return view('rehearsals.edit', compact('rehearsal', 'musicGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rehearsal  $rehearsal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rehearsal $rehearsal)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'date' => 'required',
            'duration' => 'required',
            'music-group-id' => 'required',
            'song_order' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $rehearsal->date = $data['date'];
        $rehearsal->duration = $data['duration'];
        $rehearsal->{'music-group-id'} = $data['music-group-id'];
        $rehearsal->save();

        $data['song_order'] = explode(",", $data['song_order']);

        $rehearsal->songs()->detach();
        foreach ($data['song_order'] as $idx => $song) {
            $rehearsal->songs()->save(Song::find($song), ['ordinal-number' => $idx]);
        }

        return redirect()->back()->with('status', 'Rehearsal successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rehearsal  $rehearsal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rehearsal $rehearsal)
    {
        Rehearsal::destroy($rehearsal->id);
        return redirect('rehearsals')->with('status', 'Rehearsal successfully deleted!');
    }
}
