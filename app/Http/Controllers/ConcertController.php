<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Song;
use App\Tag;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ConcertController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myGroups = \Auth::user()->musicGroups()->pluck('id')->toArray();

        if (Input::has('tag')) {
            $tags = Input::get('tag');
            $tags = explode(',', $tags);
            $concerts = Concert::whereIn('music-group-id', $myGroups)
                              ->orderBy('date', 'asc')
                              ->whereHas('tags',
                              function ($query) use ($tags){
                                  $query->whereIn('id', $tags);
                              })->paginate(10);
        } else {
            $concerts = Concert::whereIn('music-group-id', $myGroups)->orderBy('date', 'asc')->paginate(10);
        }

        $concerts->appends(Input::except(array('page')));

        return view('concerts.index', compact('concerts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $musicGroups = \Auth::user()->musicGroups()->get();
        $types = Type::get();
        $tags = Tag::orderBy('name', 'asc')->get();
        return view('concerts.create', compact('musicGroups','types','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'date' => 'required',
            'duration' => 'required',
            'location' => 'required',
            'type-id' => 'required',
            'client' => 'required',
            'contact-number' => 'required',
            'price' => 'required',
            'tags' => 'required',
            'music-group-id' => 'required',
            'song_order' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $concert = Concert::create($data);
        $data['song_order'] = explode(",", $data['song_order']);

        foreach ($data['song_order'] as $idx => $song) {
            $concert->songs()->save(Song::find($song), ['ordinal-number' => $idx]);
        }

        if(isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $concert->tags()->save(Tag::find($tag));
            }
        }

        return redirect()->back()->with('status', 'Concert successfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function show(Concert $concert)
    {
        if(!MusicGroupController::userInMusicGroup($concert->musicGroup()->first())) {
            return redirect()->back();
        }
        return view('concerts.show', compact('concert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function edit(Concert $concert)
    {
        if(!MusicGroupController::userInMusicGroup($concert->musicGroup()->first())) {
            return redirect()->back();
        }
        $musicGroups = \Auth::user()->musicGroups()->get();
        $types = Type::get();
        $tags = Tag::orderBy('name', 'asc')->get();
        $concert->date = \Carbon\Carbon::parse($concert->date)->format('Y-m-d') . "T" . \Carbon\Carbon::parse($concert->date)->format('H:i');
        $concert->duration = \Carbon\Carbon::parse($concert->duration)->format('H:i');
        return view('concerts.edit', compact('concert', 'musicGroups','types','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Concert $concert)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'date' => 'required',
            'duration' => 'required',
            'location' => 'required',
            'type-id' => 'required',
            'client' => 'required',
            'contact-number' => 'required',
            'price' => 'required',
            'tags' => 'required',
            'music-group-id' => 'required',
            'song_order' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $concert->date = $data['date'];
        $concert->duration = $data['duration'];
        $concert->location = $data['location'];
        $concert->{'type-id'} = $data['type-id'];
        $concert->client = $data['client'];
        $concert->{'contact-number'} = $data['contact-number'];
        $concert->price = $data['price'];
        $concert->{'music-group-id'} = $data['music-group-id'];
        $concert->save();

        $data['song_order'] = explode(",", $data['song_order']);

        $concert->songs()->detach();
        foreach ($data['song_order'] as $idx => $song) {
            $concert->songs()->save(Song::find($song), ['ordinal-number' => $idx]);
        }

        $concert->tags()->detach();
        if(isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $concert->tags()->save(Tag::find($tag));
            }
        }

        return redirect()->back()->with('status', 'Concert successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Concert $concert)
    {
        Concert::destroy($concert->id);
        return redirect('concerts')->with('status', 'Concert successfully deleted!');
    }
}
