<?php

namespace App\Http\Controllers;

use DB;
use App\MusicGroup;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MusicGroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public static function userInMusicGroup(MusicGroup $musicGroup) {
        return !(\Auth::user()->musicGroups()->where('id', '=', $musicGroup->id)->get()->isEmpty());
    }

    private function userIsMusicGroupAdmin(MusicGroup $musicGroup) {
        $user = \Auth::user();
        if($user == null) return false;
        return $user->id == $musicGroup->{'admin-id'};
    }

    public function invite($token)
    {
        $musicGroup = MusicGroup::where('invite-hash', '=', $token)->first();
        if(!$musicGroup) {
            return redirect('music-groups')
                    ->with('status', 'Invalid invite link!');
        }
        if($this->userInMusicGroup($musicGroup)) {
            return redirect('music-groups')
                    ->with('status', 'You are already a member of the music group \'' . $musicGroup->name . '\'!');
        }

        $data = array(
            'music-group-id' => $musicGroup->getKey(),
            'user-id' => \Auth::user()->id,
        );
        DB::table('music_group_user')->insert($data);
        $musicGroup->touch();

        return redirect('music-groups')
                  ->with('status', 'You successfully joined the music group \'' . $musicGroup->name . '\'!');
    }

    public function remove($musicGroupId, $userId) {

        $musicGroup = MusicGroup::where('id', '=', $musicGroupId)->first();

        if(!$this->userIsMusicGroupAdmin($musicGroup)) {
            return redirect()->back();
        }

        DB::table('music_group_user')
            ->where('music-group-id', '=', $musicGroupId)
            ->where('user-id', '=', $userId)
            ->delete();

        $musicGroup->touch();

        return redirect()->back()->with('status', 'Member successfully removed!');
    }

    public function setAdmin($musicGroupId, $userId) {

        $musicGroup = MusicGroup::where('id', '=', $musicGroupId)->first();

        if(!$this->userIsMusicGroupAdmin($musicGroup)) {
            return redirect()->back();
        }

        $musicGroup->update(['admin-id' => $userId]);

        return redirect()->back()->with('status', 'Administrator role successfully delegated.');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musicGroups = MusicGroup::paginate(10);
        return view('music-groups.index', compact('musicGroups'));
    }

    public function indexMy()
    {
        $musicGroups = \Auth::user()->musicGroups()->paginate(10);
        return view('music-groups.index', compact('musicGroups'));
    }

    public function getSongs(Request $request)
    {
        $data = $request->all();
        return MusicGroup::find($data['musicGroupId'])->songs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('music-groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string|min:3',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data['invite-hash'] = bin2hex(random_bytes(64));
        $data['admin-id'] = \Auth::user()->id;
        $musicGroup = MusicGroup::create($data);

        $data = array(
            'music-group-id' => $musicGroup->getKey(),
            'user-id' => \Auth::user()->id,
        );
        DB::table('music_group_user')->insert($data);

        return redirect('music-groups')->with('status', 'Music group successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function show(MusicGroup $musicGroup)
    {
        $musicGroup->isAdmin = $this->userIsMusicGroupAdmin($musicGroup);
        $users = $musicGroup->users()->get();
        return view('music-groups.show', compact('musicGroup', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function edit(MusicGroup $musicGroup)
    {
        if(!$this->userInMusicGroup($musicGroup)) {
            return redirect()->back();
        }
        return view('music-groups.edit', compact('musicGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MusicGroup $musicGroup)
    {
        if(!$this->userInMusicGroup($musicGroup)) {
            return redirect()->back();
        }

        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|string|min:3',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $musicGroup->update($data);

        return redirect('music-groups/' . $musicGroup->id )->with('status', 'Music group successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy(MusicGroup $musicGroup)
    {
        if(!$this->userInMusicGroup($musicGroup)) {
            return redirect()->back();
        }

        MusicGroup::where('id',$musicGroup->id)->delete();

        return redirect('music-groups')->with('status', 'Music group successfully deleted!');
    }
}
