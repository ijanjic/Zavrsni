<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{User, VerificationToken};
use Auth;
use App\Events\UserRequestedVerificationEmail;
class VerificationController extends Controller
{
    public function verify(VerificationToken $token)
    {
    	$token->user()->update([
    		'verified' => true
    	]);
    	$token->delete();
    	return redirect('/login')->with('status', 'Email verification succesful. Please login again!');
    }
    public function resend(Request $request)
    {
    	  $user = User::byEmail($request->email)->firstOrFail();
        if($user->hasVerifiedEmail()) {
            return redirect('/home')->with('status', 'Your email has already been verified!');
        }
        event(new UserRequestedVerificationEmail($user));
        return redirect('/login')->with('status', 'Verification email resent. Please check your inbox!');
    }
}
