<?php

namespace App\Http\Controllers;

use DB;
use App\MusicGroup;
use App\Song;
use App\SongVersion;
use App\Tag;
use App\Tonality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class SongController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Input::has('tag')) {
            $tags = Input::get('tag');
            $tags = explode(',', $tags);
            $songs = Song::whereHas('versions.tags',
              function ($query) use ($tags){
                  $query->whereIn('id', $tags);
              })->orderBy('name', 'asc')->paginate(10);
        } else if (Input::has('tonality')) {
            $tonalities = Input::get('tonality');
            $tonalities = explode(',', $tonalities);
            $songs = Song::whereHas('versions.tonalities',
              function ($query) use ($tonalities){
                  $query->whereIn('id', $tonalities);
              })->orderBy('name', 'asc')->paginate(10);
        } else if (Input::has('music-group')) {
            $musicGroups = Input::get('music-group');
            $musicGroups = explode(',', $musicGroups);
            $songs = Song::whereHas('musicGroups',
              function ($query) use ($musicGroups){
                  $query->whereIn('music-group-id', $musicGroups);
              })->orderBy('name', 'asc')->paginate(10);

        } else {
            $songs = Song::orderBy('name', 'asc')->paginate(10);
        }

        $songs->appends(Input::except(array('page')));

        foreach ($songs as $song) {
            $song->version = $song->versions()->first();
            if($song->version) {
                $song->tags = $song->version->tags()->get();
            }
        }

        return view('songs.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tonalitys = Tonality::get();
        $tags = Tag::orderBy('name', 'asc')->get();
        $musicGroups = \Auth::user()->musicGroups()->get();
        return view('songs.create', compact('tonalitys', 'tags', 'musicGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'music-group' => 'required',
            'youtube' => array(
                'required_with_all:chordify',
                'nullable',
                'regex:/(^(http|https):\/\/www\.youtube\.com\/watch\?v=.{11}$)/u'
            ),
            'chordify' => array(
                'required_with_all:youtube',
                'nullable',
                'regex:/(^(http|https):\/\/chordify\.net\/chords\/.+$)/u'
            )
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $song = Song::create($data);

        if(!empty($data['youtube'])) {
            $data['youtube'] = substr($data['youtube'], -11);
            $pos = strpos($data['chordify'], 'chords');
            $data['chordify'] = substr_replace($data['chordify'], 'embed', $pos, strlen('chords'));

            $songVersion = new SongVersion($data);
            $song->versions()->save($songVersion);

            if(isset($data['tonality'])) {
                foreach ($data['tonality'] as $tonality) {
                    $songVersion->tonalities()->save(Tonality::find($tonality));
                }
            }

            if(isset($data['tags'])) {
                foreach ($data['tags'] as $tag) {
                    $songVersion->tags()->save(Tag::find($tag));
                }
            }
        }

        foreach ($data['music-group'] as $musicGroup) {
            MusicGroup::find($musicGroup)->songs()->save($song);
        }

        return redirect()->back()->with('status', 'Song successfully added!');
    }


    private function canEdit(Song $song) {
        $user = \Auth::user();
        if($user == null) return false;
        $myGroups = $user->musicGroups()->pluck('id')->toArray();
        $songGroups = $song->musicGroups()->pluck('music_groups.id')->toArray();
        if( count(array_intersect($myGroups, $songGroups)) == 0) return false;
        return true;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function show(Song $song)
    {
        $song->edit = $this->canEdit($song);
        return view('songs.show', compact('song'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function edit(Song $song)
    {
        if( \Auth::user() == null) {
            return redirect()->back();
        }

        $edit = $this->canEdit($song);
        $musicGroups = \Auth::user()->musicGroups()->get();
        return view('songs.edit', compact('song', 'musicGroups', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Song $song)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $song->name = $data['name'];
        $song->text = $data['text'];
        $song->save();

        $musicGroups = \Auth::user()->musicGroups()->get();
        foreach ($musicGroups as $musicGroup) {
            $song->musicGroups()->detach($musicGroup);
        }

        if(isset($data['music-group'])) {
            foreach ($data['music-group'] as $musicGroup) {
                $song->musicGroups()->save(MusicGroup::find($musicGroup));
            }
        }

        return redirect('songs/' . $song->id)->with('status', 'Song successfully edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy(Song $song)
    {
        Song::destroy($song->id);
        return redirect('songs')->with('status', 'Song successfully deleted!');
    }
}
