<?php

namespace App\Http\Controllers;

use App\Song;
use App\SongVersion;
use App\Tag;
use App\Tonality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SongVersionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($songId)
    {
        $song = Song::find($songId);
        $tonalitys = Tonality::get();
        $tags = Tag::orderBy('name', 'asc')->get();
        return view('songs.versions.create', compact('song','tonalitys', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $songId)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'youtube' => array(
                'required_with_all:chordify',
                'nullable',
                'regex:/(^(http|https):\/\/www\.youtube\.com\/watch\?v=.{11}$)/u'
            ),
            'chordify' => array(
                'required_with_all:youtube',
                'nullable',
                'regex:/(^(http|https):\/\/chordify\.net\/chords\/.+$)/u'
            )
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $song = Song::find($songId);

        if(!empty($data['youtube'])) {
            $data['youtube'] = substr($data['youtube'], -11);
            $pos = strpos($data['chordify'], 'chords');
            $data['chordify'] = substr_replace($data['chordify'], 'embed', $pos, strlen('chords'));
        }

        $songVersion = new SongVersion($data);
        $song->versions()->save($songVersion);

        if(isset($data['tonality'])) {
            foreach ($data['tonality'] as $tonality) {
                $songVersion->tonalities()->save(Tonality::find($tonality));
            }
        }

        if(isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $songVersion->tags()->save(Tag::find($tag));
            }
        }

        $song->touch();

        return redirect("songs/$songId")->with('status', 'Song version successfully added!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->back();
    }

    private function canEdit(Song $song) {
        $myGroups = \Auth::user()->musicGroups()->pluck('id')->toArray();
        $songGroups = $song->musicGroups()->pluck('music_groups.id')->toArray();
        if( count(array_intersect($myGroups, $songGroups)) == 0) return false;
        return true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, SongVersion $version)
    {
        $song = Song::find($id);
        if( $song && !$this->canEdit($song) ) {
            return redirect()->back();
        }

        $tonalitys = Tonality::get();
        $tags = Tag::orderBy('name', 'asc')->get();

        if(!empty($version->youtube)) {
            $version->youtube = 'https://www.youtube.com/watch?v=' . $version->youtube;
        }
        $pos = strpos($version->chordify, 'embed');
        if ($pos !== false) {
            $version->chordify = substr_replace($version->chordify, 'chords', $pos, strlen('embed'));
        }

        return view('songs.versions.edit', compact('song', 'tonalitys', 'tags', 'version'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $songId, $songVersion)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'youtube' => array(
                'required_with_all:chordify',
                'nullable',
                'regex:/(^(http|https):\/\/www\.youtube\.com\/watch\?v=.{11}$)/u'
            ),
            'chordify' => array(
                'required_with_all:youtube',
                'nullable',
                'regex:/(^(http|https):\/\/chordify\.net\/chords\/.+$)/u'
            )
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if(!empty($data['youtube'])) {
            $data['youtube'] = substr($data['youtube'], -11);
            $pos = strpos($data['chordify'], 'chords');
            $data['chordify'] = substr_replace($data['chordify'], 'embed', $pos, strlen('chords'));
        }

        $songVersion = SongVersion::find($songVersion);
        $songVersion->youtube = $data['youtube'];
        $songVersion->chordify = $data['chordify'];
        $songVersion->save();

        $songVersion->tonalities()->detach();
        if(isset($data['tonality'])) {
            foreach ($data['tonality'] as $tonality) {
                $songVersion->tonalities()->save(Tonality::find($tonality));
            }
        }

        $songVersion->tags()->detach();
        if(isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $songVersion->tags()->save(Tag::find($tag));
            }
        }

        Song::find($songId)->touch();

        return redirect("songs/$songId")->with('status', 'Song version successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $songVersion)
    {
        SongVersion::destroy($songVersion);
        Song::find($id)->touch();
        return redirect("songs/$id")->with('status', 'Song version successfully deleted!');
    }
}
