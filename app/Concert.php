<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concert extends Model
{
    protected $fillable = [
        'date','location','duration','type-id','client','contact-number','price','music-group-id'
    ];

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'concert_song', 'concert-id', 'song-id')->withPivot('ordinal-number');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'concert_tag', 'concert-id', 'tag-id');
    }

    public function musicGroup()
    {
     return $this->belongsTo('App\MusicGroup', 'music-group-id', 'id');
    }

    public function type()
    {
     return $this->belongsTo('App\Type', 'type-id', 'id');
    }
}
