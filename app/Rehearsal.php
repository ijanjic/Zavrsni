<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rehearsal extends Model
{
    protected $fillable = [
        'date', 'duration','music-group-id'
    ];

    public function musicGroup()
    {
     return $this->belongsTo('App\MusicGroup', 'music-group-id', 'id');
    }

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'rehearsal_song', 'rehearsal-id', 'song-id')->withPivot('ordinal-number');;
    }
}
