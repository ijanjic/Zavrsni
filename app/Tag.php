<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  public function songVersions()
  {
      return $this->belongsToMany('App\SongVersion', 'song_version_tag', 'tag-id', 'song-version-id');
  }
}
