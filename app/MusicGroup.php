<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicGroup extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'name','invite-hash','admin-id'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'music_group_user', 'music-group-id', 'user-id');
    }

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'song_music_group', 'music-group-id', 'song-id');
    }
}
