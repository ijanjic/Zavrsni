@extends('layouts.app')

@section('header')
    Useful links
@endsection

@section('content')
    <p class="font-weight-bold">
        Youtube
        <a class="text-enter" href="https://www.youtube.com/">https://www.youtube.com/</a>
    </p>
    <p class="font-weight-bold">
        Chordify
        <a class="text-enter" href="https://chordify.net/">https://chordify.net/</a>
    </p>
    <p class="font-weight-bold">
        Gitare.Info
        <a class="text-enter" href="https://www.gitare.info/">https://www.gitare.info/</a>
    </p>
    <p class="font-weight-bold">
        Tekstovi.net
        <a class="text-enter" href="https://tekstovi.net/">https://tekstovi.net/</a>
    </p>
    <p class="font-weight-bold">
        Cušpajz.com
        <a class="text-enter" href="https://cuspajz.com/">https://cuspajz.com/</a>
    </p>
    <p class="font-weight-bold">
        Forum tambura
        <a class="text-enter" href="https://www.forum.tambura.com.hr/">https://www.forum.tambura.com.hr/</a>
    </p>

@endsection
