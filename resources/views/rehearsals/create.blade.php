@extends('layouts.app')

@section('header')
    Rehearsal - New rehearsal
@endsection

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script>
    $(document).ready(function() {
        $('.songs').select2();

        $(function() {
            $( "#sortable" ).disableSelection();
            $( "#sortable" ).sortable({
                update: function(event, ui) {
                    var order = $("#sortable").sortable("toArray");
                    $('#song_order').val(order.join(","));
                }
            });
        });

        $(function() {
            $(document).on("click","#sortable span.remove",function(){
                $("li#" + this.id).remove();
                var order = $("#sortable").sortable("toArray");
                $('#song_order').val(order.join(","));
            });
        });

        $(function() {
            $('.songs').change(function() {
                if($(this).val() != 0) {
                    $("#sortable").append($('<li id=' + $(this).val() + '>')
                        .append($('<table class="table table-hover">')
                            .append($('<tr>')
                                .append($('<td>')
                                    .append($('<span class="remove" id="' + $(this).val() + '">'))
                                )
                                .append($('<td>')
                                    .append($('<span class="move">'))
                                )
                                .append($('<td>')
                                    .text($('.songs option:selected').text())
                                )
                            )
                        )
                    );
                    var order = $("#sortable").sortable("toArray");
                    $('#song_order').val(order.join(","));
                }
            });
        });

        $('#music-group-id').change(function(){
            $("#sortable").empty();
            $('.songs').empty();
            $('.songs').append($('<option>', {
                value: '0',
                text : 'Choose a song...'
            }));
            var musicGroupId=$(this).val();
            $.ajax({
                url:"{{ route('musicgroup.songs')}}",
                method:'post',
                type: "POST",
                data:{musicGroupId:musicGroupId,'_token':"{{csrf_token()}}"},
                success:function(data)
                {
                    $.each(data, function (i, item) {
                      $('.songs').append($('<option>', {
                          value: item.id,
                          text : item.name
                      }));
                    });
                }
            });
        });
    });
    </script>

    <form method="POST" action="/rehearsals">
      @csrf

      <div class="form-group">
        <label for="date">Date</label>
        <input type="datetime-local" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" id="date" name="date" placeholder="Enter rehearsal date" value="{{ old('date') }}" required autofocus>
        @if ($errors->has('date'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="duration">Duration</label>
        <input type="text" pattern="([0-9]{2}):[0-5]{1}[0-9]{1}" class="form-control{{ $errors->has('duration') ? ' is-invalid' : '' }}" id="duration" name="duration" placeholder="Enter rehearsal duration" value="{{ old('duration') }}" required>
        <small id="duration" class="form-text text-muted">Example: 02:30</small>
        @if ($errors->has('duration'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('duration') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="music-group-id">Music group</label>
        <select class="form-control{{ $errors->has('music-group-id') ? ' is-invalid' : '' }}" id="music-group-id" name="music-group-id" required>
            <option disabled selected value>Choose a music group...</option>
            @foreach ($musicGroups as $musicGroup)
                <option value="{{ $musicGroup->id }}" {{ (collect(old('music-group-id'))->contains($musicGroup->id)) ? 'selected':'' }}>{{ $musicGroup->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('music-group-id'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('music-group-id') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="songs">Songs</label>
        <select class="form-control{{ $errors->has('songs') ? ' is-invalid' : '' }} songs" name="songs">
            <option disabled selected value>Choose a song...</option>
          </select>
      </div>
      <ul id="sortable"></ul>
      <input type="hidden" id="song_order" name="song_order" value="" />
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
