@extends('layouts.app')

@section('header')
    {{ \Carbon\Carbon::parse($rehearsal->date)->format('d.m.Y. - H:i:s') }}
    <span class="btn-group float-right">
        <a class="btn btn-primary float-right float-right ml-2" href="{{$rehearsal->id}}/edit" role="button">Edit</a>
        <form action="{{ $rehearsal->id }}" method="POST">
          @csrf
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger float-right ml-2">Delete</button>
        </form>
    </span>
@endsection
@section('content')
    <p class="h6 text-right font-weight-light font-italic">
        Created at: {{ \Carbon\Carbon::parse($rehearsal->created_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s')}}<br>
        Last edit: {{ \Carbon\Carbon::parse($rehearsal->updated_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s') }}
    </p>

    <p>Music group:
      <a href="/music-groups/{{$rehearsal->musicGroup->id}}">{{ $rehearsal->musicGroup->name }}</a>
    </p>

    <p>Duration:
      {{ \Carbon\Carbon::parse($rehearsal->duration)->format('H:i') }}
    </p>

    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
          @php ($i = 0)
          @foreach ($rehearsal->songs->sortBy('pivot.ordinal-number') as $index => $song)
              <tr>
                <td scope="row">{{ ++$i }}</td>
                <td><a href="/songs/{{ $song->id }}">{{ $song->name }}</a></td>
              </tr>
          @endforeach
        </tbody>
    </table>
@endsection
