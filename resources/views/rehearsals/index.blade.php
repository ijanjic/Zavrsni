@extends('layouts.app')

@section('header')
    Rehearsals - List
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Date</th>
          <th scope="col">Duration</th>
          <th scope="col">Music group</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($rehearsals as $index => $rehearsal)
              <tr>
                <th scope="row">{{ $index + $rehearsals->firstItem() }}</th>
                <td><a href="/rehearsals/{{ $rehearsal->id }}">{{ \Carbon\Carbon::parse($rehearsal->date)->format('d.m.Y. - H:i')}}</a></td>
                <td>{{ \Carbon\Carbon::parse($rehearsal->duration)->format('H:i') }}</td>
                <td><a href="/music-groups/{{ $rehearsal->{'music-group-id'} }}">{{ $rehearsal->musicGroup->name }}</a></td>
              </tr>
          @endforeach
        </tbody>
    </table>

    {{ $rehearsals->links() }}
@endsection
