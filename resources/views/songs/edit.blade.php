@extends('layouts.app')

@section('header')
    Songs - Edit song
@endsection

@section('content')
    <form action="/songs/{{ $song->id }}" method="POST">
      @csrf
      {{ method_field('PUT') }}

      @if ($edit)
      <div class="form-group">
        <label for="name">Song name</label>
        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter song name" value="{{ old('name', $song->name) }}" required autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>
      @else
      <input type="hidden" id="name" name="name" value="{{ $song->name }}">
      @endif
      <div class="form-group">
        <label for="music-group">Music group</label>
        <select class="form-control{{ $errors->has('music-group') ? ' is-invalid' : '' }}" id="music-group" name="music-group[]" multiple>
            @foreach ($musicGroups as $musicGroup)
                <option value="{{ $musicGroup->id }}" {{ (collect(old('music-group', $song->musicGroups()->pluck('music_groups.id')))->contains($musicGroup->id)) ? 'selected':'' }}>{{ $musicGroup->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('music-group'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('music-group') }}</strong>
            </span>
        @endif
      </div>
      @if ($edit)
      <div class="form-group">
        <label for="text">Text</label>
        <textarea class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" id="text" name="text" rows="10">{{ old('text', $song->text) }}</textarea>
        @if ($errors->has('text'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('text') }}</strong>
            </span>
        @endif
      </div>
      @else
      <input type="hidden" id="text" name="text" value="{{ $song->text }}">
      @endif
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
