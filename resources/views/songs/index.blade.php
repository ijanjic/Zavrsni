@extends('layouts.app')

@section('header')
    Songs - List
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Youtube</th>
          <th scope="col">Tags</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($songs as $index => $song)
              <tr>
                <th scope="row">{{ $index + $songs->firstItem() }}</th>
                <td><a href="/songs/{{ $song->id }}">{{ $song->name }}</a></td>
                @if ($song->version && $song->version->youtube)
                    <td><a href="https://www.youtube.com/watch?v={{ $song->version->youtube }}" target="_blank"><img src="https://img.youtube.com/vi/{{ $song->version->youtube }}/2.jpg"></a></td>
                @else
                    <td><a href="https://www.youtube.com/watch?v=" target="_blank"><img src="https://img.youtube.com/vi//2.jpg"></a></td>
                @endif
                @if ($song->tags)
                    <td>
                        @foreach ($song->tags as $tag)
                            <a href="/songs?tag={{ $tag->id }}">{{ $tag->name }}</a>
                            <br>
                        @endforeach
                    </td>
                @endif
              </tr>
          @endforeach
        </tbody>
    </table>
    {{ $songs->links() }}
@endsection
