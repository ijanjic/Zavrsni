@extends('layouts.app')

@section('header')
    Songs - New song
@endsection

@section('content')
    <form method="POST" action="/songs">
      @csrf

      <div class="form-group">
        <label for="name">Song name</label>
        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter song name" value="{{ old('name') }}" required autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="music-group">Music group</label>
        <select class="form-control{{ $errors->has('music-group') ? ' is-invalid' : '' }}" id="music-group" name="music-group[]" multiple required>
            @foreach ($musicGroups as $musicGroup)
                <option value="{{ $musicGroup->id }}" {{ (collect(old('music-group'))->contains($musicGroup->id)) ? 'selected':'' }}>{{ $musicGroup->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('music-group'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('music-group') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="youtube">Youtube</label>
        <input type="text" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}" id="youtube" name="youtube" aria-describedby="youtubeHelp" placeholder="Enter youtube link" value="{{ old('youtube') }}">
        <small id="youtube" class="form-text text-muted">Example: https://www.youtube.com/watch?v=6QZYc-Dublg</small>
        @if ($errors->has('youtube'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('youtube') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="chordify">Chordify</label>
        <input type="text" class="form-control{{ $errors->has('chordify') ? ' is-invalid' : '' }}" id="chordify" name="chordify" aria-describedby="chordifyHelp" placeholder="Enter chordify link" value="{{ old('chordify') }}">
        <small id="chordify" class="form-text text-muted">Example: https://chordify.net/chords/ts-diversa-zadnja-zelja-tamburaski-sastav-diversa</small>
        @if ($errors->has('chordify'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('chordify') }}</strong>
            </span>
        @endif
      </div>
      <div>
        <label for="tonality">Tonality</label>
        <select class="form-control" id="tonality" name="tonality[]" multiple>
          @foreach ($tonalitys as $tonality)
              <option value="{{ $tonality->id }}" {{ (collect(old('tonality'))->contains($tonality->id)) ? 'selected':'' }}>{{ $tonality->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('tonality'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('tonality') }}</strong>
            </span>
        @endif
      </div>
      <div>
        <label for="tags">Tags</label>
        <select class="form-control" id="tags" name="tags[]" multiple>
          @foreach ($tags as $tag)
              <option value="{{ $tag->id }}" {{ (collect(old('tags'))->contains($tag->id)) ? 'selected':'' }}>{{ $tag->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('tags'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('tags') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="text">Text</label>
        <textarea class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" id="text" name="text" rows="10">{{ old('text') }}</textarea>
        @if ($errors->has('text'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('text') }}</strong>
            </span>
        @endif
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
