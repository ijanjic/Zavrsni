@extends('layouts.app')

@section('header')
    {{ $song->name }}
    <span class="btn-group float-right">
            <a class="btn btn-primary float-right ml-2" href="{{$song->id}}/versions/create" role="button">Add version</a>
            <a class="btn btn-primary float-right ml-2" href="{{$song->id}}/edit" role="button">Edit</a>
        @if ($song->edit)
            <form action="{{ $song->id }}" method="POST">
              @csrf
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger float-right ml-2">Delete</button>
            </form>
        @endif
    </span>
@endsection

@section('content')
    <p class="h6 text-right font-weight-light font-italic">
        Created at: {{ \Carbon\Carbon::parse($song->created_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s')}}<br>
        Last edit: {{ \Carbon\Carbon::parse($song->updated_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s') }}
    </p>

    <p>Music groups:
      @foreach ($song->musicGroups as $musicGroup)
          <a href="/music-groups/{{$musicGroup->id}}">{{ $musicGroup->name }}</a>
      @endforeach
    </p>

    @auth
        @if (!empty($song->text))
            <h5 style="white-space: pre-wrap;" class="text-center font-weight-bold mt-5">{{ $song->text }}</h5>
        @endif
    @endauth

    <div class="row justify-content-center">
        <div class="col-md-9">

    @foreach ($song->versions as $index => $version)
        <div class="card mt-3">
            <div class="card-header">Version {{ $index+1 }}
                @if ($song->edit)
                    <span class="btn-group float-right">
                        <a class="btn btn-primary float-right float-right ml-2" href="{{$song->id}}/versions/{{ $version->id}}/edit" role="button">Edit</a>
                        <form action="{{ $song->id }}/versions/{{ $version->id }}" method="POST">
                          @csrf
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger float-right ml-2">Delete</button>
                        </form>
                    </span>
                @endif
            </div>
            <div class="card-body">
                @auth
                    @if (count($version->tonalities) > 0)
                        <p>Tonality:
                            @foreach ($version->tonalities as $tonality)
                                <a href="/songs?tonality={{ $tonality->id }}">{{ $tonality->name }}</a>
                            @endforeach
                        </p>
                    @endif
                @endauth

                @if (count($version->tags) > 0)
                    <p>Tags:
                        @foreach ($version->tags as $tag)
                            <a href="/songs?tag={{ $tag->id }}">{{ $tag->name }}</a>
                        @endforeach
                    </p>
                @endif

                @if (!empty($version->youtube))
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/{{ $version->youtube }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                @endif

                @auth
                    @if (!empty($version->chordify))
                        <iframe class="mt-5" src="{{ $version->chordify }}" frameborder="0" width="100%" height="500"></iframe>
                    @endif
                @endauth
            </div>
        </div>
    @endforeach
  </div></div>
@endsection
