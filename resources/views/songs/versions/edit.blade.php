@extends('layouts.app')

@section('header')
    Song versions - Edit song version
@endsection
@section('content')
    <form action="/songs/{{ $song->id }}/versions/{{ $version->id }}" method="POST">
      @csrf
      {{ method_field('PUT') }}

      <div class="form-group">
        <label for="youtube">Youtube</label>
        <input type="text" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}" id="youtube" name="youtube" aria-describedby="youtubeHelp" placeholder="Enter youtube link" value="{{ old('youtube', $version->youtube) }}">
        <small id="youtube" class="form-text text-muted">Example: https://www.youtube.com/watch?v=6QZYc-Dublg</small>
        @if ($errors->has('youtube'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('youtube') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="chordify">Chordify</label>
        <input type="text" class="form-control{{ $errors->has('chordify') ? ' is-invalid' : '' }}" id="chordify" name="chordify" aria-describedby="chordifyHelp" placeholder="Enter chordify link" value="{{ old('chordify', $version->chordify) }}">
        <small id="chordify" class="form-text text-muted">Example: https://chordify.net/chords/ts-diversa-zadnja-zelja-tamburaski-sastav-diversa</small>
        @if ($errors->has('chordify'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('chordify') }}</strong>
            </span>
        @endif
      </div>
      <div>
        <label for="tonality">Tonality</label>
        <select class="form-control" id="tonality" name="tonality[]" multiple>
          @foreach ($tonalitys as $tonality)
              <option value="{{ $tonality->id }}" {{ in_array($tonality->id, $version->tonalities()->pluck('id')->toArray() ) ? 'selected' : '' }}>{{ $tonality->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('tonality'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('tonality') }}</strong>
            </span>
        @endif
      </div>
      <div>
        <label for="tags">Tags</label>
        <select class="form-control" id="tags" name="tags[]" multiple>
          @foreach ($tags as $tag)
              <option value="{{ $tag->id }}" {{ in_array($tag->id, $version->tags()->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('tags'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('tags') }}</strong>
            </span>
        @endif
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
