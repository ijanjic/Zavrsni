@extends('layouts.app')

@section('header')
    Music groups - Edit music group
@endsection

@section('content')
    <form action="/music-groups/{{ $musicGroup->id }}" method="POST">
      @csrf
      {{ method_field('PUT') }}

      <div class="form-group">
        <label for="name">Music group name</label>
        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter music group name" value="{{ old('name', $musicGroup->name) }}" required autofocus>
        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
