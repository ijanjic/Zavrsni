@extends('layouts.app')

@section('header')
    My music groups
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($musicGroups as $index => $musicGroup)
              <tr>
                <th scope="row">{{ $index + $musicGroups->firstItem() }}</th>
                <td><a href="/music-groups/{{ $musicGroup->id }}">{{ $musicGroup->name }}</a></td>
              </tr>
          @endforeach
        </tbody>
    </table>

    {{ $musicGroups->links() }}
@endsection
