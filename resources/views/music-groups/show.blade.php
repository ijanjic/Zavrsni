@extends('layouts.app')

@section('header')
    {{ $musicGroup->name }}
    @if ($musicGroup->isAdmin)
        <span class="btn-group float-right">
            <a class="btn btn-primary float-right" href="{{$musicGroup->id}}/edit" role="button">Edit</a>
            <form action="{{ $musicGroup->id }}" method="POST">
              @csrf
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger float-right ml-2">Delete</button>
            </form>
        </span>
    @endif
@endsection

@section('content')
    <script>
        function myFunction() {
            /* Get the text field */
            var copyText = document.getElementById("myInput");

            /* Select the text field */
            copyText.select();

            /* Copy the text inside the text field */
            document.execCommand("Copy");
        }
    </script>

    <p class="h6 text-right font-weight-light font-italic">
        Created at: {{ \Carbon\Carbon::parse($musicGroup->created_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s')}}<br>
        Last edit: {{ \Carbon\Carbon::parse($musicGroup->updated_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s') }}
    </p>

    <a class="btn btn-primary" href="/songs?music-group={{$musicGroup->id}}" role="button">Music group songs</a>

    @if ($musicGroup->isAdmin)
        <div class="input-group mt-5">
           <input type="text" class="form-control" value="{{ URL::to('/') }}/music-groups/invite/{{ $musicGroup->{'invite-hash'} }}" id="myInput">
           <span class="input-group-btn">
             <button class="btn btn-primary ml-2" onclick="myFunction()">Copy invite link</button>
           </span>
        </div>
    @endif

    <h4 class="mt-5">Members:</h4>

    <ul class="list-group">
      @foreach($users as $user)
          <li class="list-group-item">{{ $user->name }}
              @auth
                  @if ($user->id == $musicGroup->{'admin-id'})
                      <span class="float-right">Administrator</span>
                  @elseif ($musicGroup->isAdmin)
                      <span class="btn-group float-right">
                          <form action="{{ $musicGroup->id }}/set-admin/{{$user->id}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary float-right">Delegate administrator role</button>
                          </form>
                          <form action="{{ $musicGroup->id }}/remove/{{$user->id}}" method="POST">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger float-right ml-2">Remove</button>
                          </form>
                      </span>
                  @endif
              @endauth
          </li>
      @endforeach
    </ul>
@endsection
