@extends('layouts.app')

@section('header')
    {{ \Carbon\Carbon::parse($concert->date)->format('d.m.Y. - H:i:s') }}
    <span class="btn-group float-right">
        <a class="btn btn-primary float-right float-right ml-2" href="{{$concert->id}}/edit" role="button">Edit</a>
        <form action="{{ $concert->id }}" method="POST">
          @csrf
          {{ method_field('DELETE') }}
          <button type="submit" class="btn btn-danger float-right ml-2">Delete</button>
        </form>
    </span>
@endsection

@section('content')
    <p class="h6 text-right font-weight-light font-italic">
        Created at: {{ \Carbon\Carbon::parse($concert->created_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s')}}<br>
        Last edit: {{ \Carbon\Carbon::parse($concert->updated_at)->setTimezone('Europe/Zagreb')->format('d.m.Y. - H:i:s') }}
    </p>

    <p>Music group:
      <a href="/music-groups/{{$concert->musicGroup->id}}">{{ $concert->musicGroup->name }}</a>
    </p>

    <p>Duration:
      {{ \Carbon\Carbon::parse($concert->duration)->format('H:i') }}
    </p>

    <p>Location:
      {{ $concert->location }}
    </p>

    <p>Client:
      {{ $concert->client }}
    </p>

    <p>Contact number:
      {{ $concert->{'contact-number'} }}
    </p>

    <p>Price:
      {{ $concert->price }} kn
    </p>

    <p>Concert type:
      {{ $concert->type->name }}
    </p>

    @if (count($concert->tags) > 0)
        <p>Tags:
            @foreach ($concert->tags as $tag)
                <a href="/concerts?tag={{ $tag->id }}">{{ $tag->name }}</a>
            @endforeach
        </p>
    @endif

    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
          @php ($i = 0)
          @foreach ($concert->songs->sortBy('pivot.ordinal-number') as $index => $song)
              <tr>
                <td scope="row">{{ ++$i }}</td>
                <td><a href="/songs/{{ $song->id }}">{{ $song->name }}</a></td>
              </tr>
          @endforeach
        </tbody>
    </table>
@endsection
