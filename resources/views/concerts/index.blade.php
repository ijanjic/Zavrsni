@extends('layouts.app')

@section('header')
    Concerts - List
@endsection

@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Date</th>
          <th scope="col">Location</th>
          <th scope="col">Music group</th>
          <th scope="col">Tags</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($concerts as $index => $concert)
              <tr>
                <td scope="row">{{ $index + $concerts->firstItem() }}</td>
                <td><a href="/concerts/{{ $concert->id }}">{{ \Carbon\Carbon::parse($concert->date)->format('d.m.Y. - H:i')}}</a></td>
                <td>{{$concert->location}}</td>
                <td><a href="/music-groups/{{ $concert->{'music-group-id'} }}">{{ $concert->musicGroup->name }}</a></td>
                @if ($concert->tags)
                    <td>
                        @foreach ($concert->tags as $tag)
                            <a href="/concerts?tag={{ $tag->id }}">{{ $tag->name }}</a>
                            <br>
                        @endforeach
                    </td>
                @endif
              </tr>
          @endforeach
        </tbody>
    </table>

    {{ $concerts->links() }}
@endsection
