@extends('layouts.app')

@section('header')
    Concerts - Edit concert
@endsection

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script>
    $(document).ready(function() {
        $('.songs').select2();

        $(function() {
            $( "#sortable" ).disableSelection();
            $( "#sortable" ).sortable({
                update: function(event, ui) {
                    var order = $("#sortable").sortable("toArray");
                    $('#song_order').val(order.join(","));
                }
            });
        });

        $(function() {
            $(document).on("click","#sortable span.remove",function(){
                $("li#" + this.id).remove();
                var order = $("#sortable").sortable("toArray");
                $('#song_order').val(order.join(","));
            });
        });

        $(function() {
            $('.songs').change(function() {
                if($(this).val() != 0) {
                    $("#sortable").append($('<li id=' + $(this).val() + '>')
                        .append($('<table class="table table-hover">')
                            .append($('<tr>')
                                .append($('<td>')
                                    .append($('<span class="remove" id="' + $(this).val() + '">'))
                                )
                                .append($('<td>')
                                    .append($('<span class="move">'))
                                )
                                .append($('<td>')
                                    .text($('.songs option:selected').text())
                                )
                            )
                        )
                    );
                    var order = $("#sortable").sortable("toArray");
                    $('#song_order').val(order.join(","));
                }
            });
        });

        $('#music-group-id').change(function(){
            $("#sortable").empty();
            $('.songs').empty();
            $('.songs').append($('<option>', {
                value: '0',
                text : 'Choose a song...'
            }));
            var musicGroupId=$(this).val();
            $.ajax({
                url:"{{ route('musicgroup.songs')}}",
                method:'post',
                type: "POST",
                data:{musicGroupId:musicGroupId,'_token':"{{csrf_token()}}"},
                success:function(data)
                {
                    $.each(data, function (i, item) {
                      $('.songs').append($('<option>', {
                          value: item.id,
                          text : item.name
                      }));
                    });
                }
            });
        });

        $('#music-group-id').change();

        var order = [];
        @foreach($concert->songs()->orderBy('concert_song.ordinal-number', 'asc')->get() as $idx => $song)
            $("#sortable").append($('<li id=' + "{{$song->id}}" + '>')
                .append($('<table class="table table-hover">')
                    .append($('<tr>')
                        .append($('<td>')
                            .append($('<span class="remove" id="' + "{{$song->id}}" + '">'))
                        )
                        .append($('<td>')
                            .append($('<span class="move">'))
                        )
                        .append($('<td>')
                            .text("{{$song->name}}")
                        )
                    )
                )
            );
            order[{{$idx}}] = {{$song->id}};
        @endforeach
        $('#song_order').val(order.join(","));

    });
    </script>

    <form action="/concerts/{{ $concert->id }}" method="POST">
      @csrf
      {{ method_field('PUT') }}

      <div class="form-group">
        <label for="date">Date</label>
        <input type="datetime-local" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" id="date" name="date" placeholder="Enter concert date" value="{{ old('date', $concert->date ) }}" required autofocus>
        @if ($errors->has('date'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="duration">Duration</label>
        <input type="text" pattern="([0-9]{2}):[0-5]{1}[0-9]{1}" class="form-control{{ $errors->has('duration') ? ' is-invalid' : '' }}" id="duration" name="duration" placeholder="Enter concert duration" value="{{ old('duration', $concert->duration ) }}" required>
        <small id="duration" class="form-text text-muted">Example: 02:30</small>
        @if ($errors->has('duration'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('duration') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="location">Location</label>
        <input type="text" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" id="location" name="location" placeholder="Enter concert location" value="{{ old('location',$concert->location) }}" required>
        <small id="location" class="form-text text-muted">Example: Unska 3, Zagreb</small>
        @if ($errors->has('location'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="type-id">Concert type</label>
        <select class="form-control{{ $errors->has('type-id') ? ' is-invalid' : '' }}" id="type-id" name="type-id" required>
            <option disabled selected value>Choose a type...</option>
            @foreach ($types as $type)
                <option value="{{ $type->id }}" {{ (collect(old('type-id', $concert->type->id))->contains($type->id)) ? 'selected':'' }}>{{ $type->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('type-id'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('type-id') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="client">Client</label>
        <input type="text" class="form-control{{ $errors->has('client') ? ' is-invalid' : '' }}" id="client" name="client" placeholder="Enter client" value="{{ old('client', $concert->client) }}" required>
        <small id="client" class="form-text text-muted">Example: Marko Markić</small>
        @if ($errors->has('client'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('client') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="contact-number">Contact number</label>
        <input type="text" class="form-control{{ $errors->has('contact-number') ? ' is-invalid' : '' }}" id="contact-number" name="contact-number" placeholder="Enter contact number" value="{{ old('contact-number', $concert->{'contact-number'}) }}" required>
        <small id="contact-number" class="form-text text-muted">Example: +385 1 6123 456</small>
        @if ($errors->has('contact-number'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('contact-number') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="price">Price (kn)</label>
        <input type="number" min="0" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price" name="price" placeholder="Enter price" value="{{ old('price', $concert->price) }}" required>
        @if ($errors->has('price'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
      </div>
      <div>
        <label for="tags">Tags</label>
        <select class="form-control" id="tags" name="tags[]" multiple>
          @foreach ($tags as $tag)
              <option value="{{ $tag->id }}" {{ in_array($tag->id, $concert->tags()->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $tag->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('tags'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('tags') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="music-group-id">Music group</label>
        <select class="form-control{{ $errors->has('music-group-id') ? ' is-invalid' : '' }}" id="music-group-id" name="music-group-id" required>
            <option disabled selected value>Choose a music group...</option>
            @foreach ($musicGroups as $musicGroup)
                <option value="{{ $musicGroup->id }}" {{ (collect(old('music-group-id', $concert->musicGroup))->contains($musicGroup->id)) ? 'selected':'' }}>{{ $musicGroup->name }}</option>
            @endforeach
        </select>
        @if ($errors->has('music-group-id'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('music-group-id') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group">
        <label for="songs">Songs</label>
        <select class="form-control{{ $errors->has('songs') ? ' is-invalid' : '' }} songs" name="songs">
            <option disabled selected value>Choose a song...</option>
          </select>
      </div>
      <ul id="sortable"></ul>
      <input type="hidden" id="song_order" name="song_order" value="" />
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
