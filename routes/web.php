<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/verify/token/{token}', 'Auth\VerificationController@verify')->name('auth.verify');
Route::get('/verify/resend', 'Auth\VerificationController@resend')->name('auth.verify.resend');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/useful-links', 'HomeController@links');

Route::get('/music-groups/my', 'MusicGroupController@indexMy');
Route::get('/music-groups/invite/{token}', 'MusicGroupController@invite');
Route::delete('/music-groups/{musicGroupId}/remove/{userId}', 'MusicGroupController@remove');
Route::post('/music-groups/{musicGroupId}/set-admin/{userId}', 'MusicGroupController@setAdmin');
Route::post('/music-groups/songs', 'MusicGroupController@getSongs')->name('musicgroup.songs');

Route::resource('songs', 'SongController');
Route::resource('songs/{songId}/versions', 'SongVersionController');
Route::resource('music-groups', 'MusicGroupController');
Route::resource('rehearsals', 'RehearsalController');
Route::resource('concerts', 'ConcertController');
